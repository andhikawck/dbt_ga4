FROM python:3.8

# Allow statements and log messages to immediately appear in the Knative logs
ENV PYTHONUNBUFFERED True

# Copy local code to the container image.
ENV APP_HOME /app
WORKDIR $APP_HOME
COPY . ./

# Install production depedencies
RUN apt-get -y update
RUN pip install --upgrade pip
RUN apt-get install -y xvfb gnupg wget curl unzip 
RUN pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib google-cloud-bigquery google-cloud-storage
RUN pip install -r requirements.txt

FROM ghcr.io/dbt-labs/dbt-bigquery:1.2.latest
USER root
WORKDIR /dbt
COPY --from=builder /app/server ./
COPY script.sh ./
COPY . ./

# Run API
CMD exec gunicorn --bind :8080 --workers 1 --threads 8 --timeout 0 app:app