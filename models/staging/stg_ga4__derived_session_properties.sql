{{
    config(
        enabled=true if var("derived_session_properties", false) else false,
        materialized="table",
    )
}}

-- Remove null session_keys (users with privacy enabled)
with
    events_from_valid_users as (
        select * from {{ ref("stg_ga4__events") }} where session_key is not null
    ),
    unnest_event_params as (
        select
            session_key,
            event_timestamp
            {% for sp in var("derived_session_properties", []) %}
            {% if sp.user_property %}
            , {{ ga4.unnest_key("user_properties", sp.user_property, sp.value_type) }}
            {% else %}
            , {{ ga4.unnest_key("event_params", sp.event_parameter, sp.value_type) }}
            {% endif %}
            {% endfor %}
        from events_from_valid_users
    )

select distinct
    session_key
    {% for sp in var("derived_session_properties", []) %}
    ,
    last_value({{ sp.user_property | default(sp.event_parameter) }} ignore nulls) over (
        session_window
    ) as {{ sp.session_property_name }}
    {% endfor %}
from unnest_event_params
window
    session_window as (
        partition by session_key
        order by event_timestamp
        rows between unbounded preceding and unbounded following
    )
