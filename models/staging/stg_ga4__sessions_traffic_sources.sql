with
    session_events as (
        select
            session_key,
            event_timestamp,
            lower(source) as source,
            medium,
            campaign,
            content,
            term,
            source_category
        from {{ ref("stg_ga4__events") }}
        left join {{ ref("ga4_source_categories") }} using (source)
        where
            session_key is not null
            and event_name != 'session_start'
            and event_name != 'first_visit'
    ),
    set_default_channel_grouping as (
        select
            *,
            {{ ga4.default_channel_grouping("source", "medium", "source_category") }}
            as default_channel_grouping
        from session_events
    ),
    session_source as (
        select
            session_key,
            coalesce(
                first_value(
                    (case when source <> '(direct)' then source end) ignore nulls
                ) over (session_window),
                '(direct)'
            ) as source,
            coalesce(
                first_value(
                    (
                        case
                            when source <> '(direct)' then coalesce(medium, '(none)')
                        end
                    ) ignore nulls
                ) over (session_window),
                '(none)'
            ) as medium,
            coalesce(
                first_value(
                    (
                        case
                            when source <> '(direct)'
                            then coalesce(source_category, '(none)')
                        end
                    ) ignore nulls
                ) over (session_window),
                '(none)'
            ) as source_category,
            coalesce(
                first_value(
                    (
                        case
                            when source <> '(direct)' then coalesce(campaign, '(none)')
                        end
                    ) ignore nulls
                ) over (session_window),
                '(none)'
            ) as campaign,
            coalesce(
                first_value(
                    (
                        case
                            when source <> '(direct)' then coalesce(content, '(none)')
                        end
                    ) ignore nulls
                ) over (session_window),
                '(none)'
            ) as content,
            coalesce(
                first_value(
                    (
                        case when source <> '(direct)' then coalesce(term, '(none)') end
                    ) ignore nulls
                ) over (session_window),
                '(none)'
            ) as term,
            coalesce(
                first_value(
                    (
                        case
                            when source <> '(direct)'
                            then coalesce(default_channel_grouping, '(none)')
                        end
                    ) ignore nulls
                ) over (session_window),
                '(none)'
            ) as default_channel_grouping
        from set_default_channel_grouping
        window
            session_window as (
                partition by session_key
                order by event_timestamp asc
                rows between unbounded preceding and unbounded following
            )
    )

select distinct *
from session_source
