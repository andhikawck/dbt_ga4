{{
    config(
        enabled=true if var("derived_user_properties", false) else false,
        materialized="table",
    )
}}

-- Remove null user_pseudo_id (users with privacy enabled)
with
    events_from_valid_users as (
        select * from {{ ref("stg_ga4__events") }} where user_pseudo_id is not null
    ),
    unnest_user_properties as (
        select
            user_pseudo_id,
            event_timestamp
            {% for up in var("derived_user_properties", []) %}
            ,{{ ga4.unnest_key("event_params", up.event_parameter, up.value_type) }}
            {% endfor %}
        from events_from_valid_users
    )

select distinct
    user_pseudo_id
    {% for up in var("derived_user_properties", []) %}
    ,
    last_value({{ up.event_parameter }} ignore nulls) over (
        user_window
    ) as {{ up.user_property_name }}
    {% endfor %}
from unnest_user_properties
window
    user_window as (
        partition by user_pseudo_id
        order by event_timestamp
        rows between unbounded preceding and unbounded following
    )
