from flask import Flask

import os
import subprocess
import logging

from utils.validation import write_key_required

app = Flask(__name__)

@app.route('/dbt')
@write_key_required
def dbt_run():
    execute_script()

def execute_script():
    try:
        subprocess.run(['/bin/sh', 'script.sh'], check=True)
    except subprocess.CalledProcessError as e:
        logging.error(f"cmd.Run() failed with {e}")
        raise SystemExit

if __name__ == '__main__':
    port = os.environ.get('PORT', 8080)
    app.run(host='0.0.0.0', port=port)
