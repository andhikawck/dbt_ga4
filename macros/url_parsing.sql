{% macro extract_hostname_from_url(url) %}
regexp_extract({{ url }}, '(?:http[s]?://)?(?:www\\.)?(.*?)(?:(?:/|:)(?:.)*|$)')
{% endmacro %}

{% macro extract_query_string_from_url(url) %}
regexp_extract({{ url }}, '\\?(.+)')
{% endmacro %}

{% macro remove_query_parameters(url, parameters) %}
regexp_replace(
    regexp_replace(
        regexp_replace(
            regexp_replace(
                {{ url }}, '(\\?|&)({{ parameters|join("|") }})=[^&]*', '\\1'
            ),
            '\\?&+',
            '?'
        ),
        '&+',
        '&'
    ),
    '\\?$|&$',
    ''
)
{% endmacro %}

{% macro extract_page_path(url) %}
regexp_extract({{ url }}, '(?:\\w+:)?\\/\\/[^\\/]+([^?#]+)')
{% endmacro %}
