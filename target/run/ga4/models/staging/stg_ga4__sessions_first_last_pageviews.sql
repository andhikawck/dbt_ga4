

  create or replace view `merkle-id-cdp`.`dbt_test_staging`.`stg_ga4__sessions_first_last_pageviews`
  OPTIONS()
  as with
    page_views_first_last as (
        select
            session_key,
            first_value(event_key) over (
                partition by session_key
                order by event_timestamp
                rows between unbounded preceding and unbounded following
            ) as first_page_view_event_key,
            last_value(event_key) over (
                partition by session_key
                order by event_timestamp
                rows between unbounded preceding and unbounded following
            ) as last_page_view_event_key
        from `merkle-id-cdp`.`dbt_test_staging`.`stg_ga4__events`
        where event_name = 'page_view'
    ),
    page_views_by_session_key as (
        select distinct session_key, first_page_view_event_key, last_page_view_event_key
        from page_views_first_last
    )

select *
from page_views_by_session_key;

