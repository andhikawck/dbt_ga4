select
      count(*) as failures,
      count(*) != 0 as should_warn,
      count(*) != 0 as should_error
    from (
      
    
    

with dbt_test__target as (

  select session_key as unique_field
  from `merkle-id-cdp`.`dbt_test_staging`.`stg_ga4__sessions_traffic_sources`
  where session_key is not null

)

select
    unique_field,
    count(*) as n_records

from dbt_test__target
group by unique_field
having count(*) > 1



      
    ) dbt_internal_test