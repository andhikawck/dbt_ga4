

  create or replace view `merkle-id-cdp`.`dbt_test_staging`.`stg_ga4__event_first_visit`
  OPTIONS()
  as -- TODO: Unclear why there are first_visit events firing when the ga_session_number is
-- >1. This might cause confusion.
with
    first_visit_with_params as (
        select
            *,
            (
    select value.string_value
    from unnest(event_params)
    where key = 'page_location'
) as landing_page
            
            
        from `merkle-id-cdp`.`dbt_test_staging`.`stg_ga4__events`
        where event_name = 'first_visit'
    )

select *
from first_visit_with_params;

