
   
      -- generated script to merge partitions into `merkle-id-cdp`.`dbt_test_staging`.`base_ga4__events`
      declare dbt_partitions_for_replacement array<date>;

      
      
        declare _dbt_max_partition date default (
      select max(event_date_dt) from `merkle-id-cdp`.`dbt_test_staging`.`base_ga4__events`
      where event_date_dt is not null
    );

       -- 1. create a temp table with model data
        
  
    

    create or replace table `merkle-id-cdp`.`dbt_test_staging`.`base_ga4__events__dbt_tmp`
    partition by event_date_dt
    cluster by event_name

    OPTIONS(
      expiration_timestamp=TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 12 hour)
    )
    as (
      


-- BigQuery does not cache wildcard queries that scan across sharded tables which
-- means it's best to materialize the raw event data as a partitioned table so that
-- future queries benefit from caching
with
    source as (
        select
            parse_date('%Y%m%d', event_date) as event_date_dt,
            event_timestamp,
            event_name,
            event_params,
            event_previous_timestamp,
            event_value_in_usd,
            event_bundle_sequence_id,
            event_server_timestamp_offset,
            user_id,
            user_pseudo_id,
            privacy_info,
            user_properties,
            user_first_touch_timestamp,
            user_ltv,
            device,
            geo,
            app_info,
            traffic_source,
            stream_id,
            platform,
            ecommerce,
            items,
        
        from `merkle-id-cdp`.`dbt_test`.`events_*`
        where
            _table_suffix not like '%intraday%'
            and cast(_table_suffix as int64) >= 20210131
        
            

            
            -- Incrementally add new events. Filters on _TABLE_SUFFIX using the max
            -- event_date_dt value found in `merkle-id-cdp`.`dbt_test_staging`.`base_ga4__events`
            -- See
            -- https://docs.getdbt.com/reference/resource-configs/bigquery-configs#the-insert_overwrite-strategy
            and parse_date('%Y%m%d', _table_suffix) >= _dbt_max_partition
            
            
    ),
    renamed as (
        select
            event_date_dt,
            event_timestamp,
            lower(replace(trim(event_name), " ", "_")) as event_name,  -- Clean up all event names to be snake cased
            event_params,
            event_previous_timestamp,
            event_value_in_usd,
            event_bundle_sequence_id,
            event_server_timestamp_offset,
            user_id,
            user_pseudo_id,
            privacy_info.analytics_storage as privacy_info_analytics_storage,
            privacy_info.ads_storage as privacy_info_ads_storage,
            privacy_info.uses_transient_token as privacy_info_uses_transient_token,
            user_properties,
            user_first_touch_timestamp,
            user_ltv.revenue as user_ltv_revenue,
            user_ltv.currency as user_ltv_currency,
            device.category as device_category,
            device.mobile_brand_name as device_mobile_brand_name,
            device.mobile_model_name as device_mobile_model_name,
            device.mobile_marketing_name as device_mobile_marketing_name,
            device.mobile_os_hardware_model as device_mobile_os_hardware_model,
            device.operating_system as device_operating_system,
            device.operating_system_version as device_operating_system_version,
            device.vendor_id as device_vendor_id,
            device.advertising_id as device_advertising_id,
            device.language as device_language,
            device.is_limited_ad_tracking as device_is_limited_ad_tracking,
            device.time_zone_offset_seconds as device_time_zone_offset_seconds,
            device.web_info.browser as device_web_info_browser,
            device.web_info.browser_version as device_web_info_browser_version,
            geo.continent as geo_continent,
            geo.country as geo_country,
            geo.region as geo_region,
            geo.city as geo_city,
            geo.sub_continent as geo_sub_continent,
            geo.metro as geo_metro,
            app_info.id as app_info_id,
            app_info.version as app_info_version,
            app_info.install_store as app_info_install_store,
            app_info.firebase_app_id as app_info_firebase_app_id,
            app_info.install_source as app_info_install_source,
            traffic_source.name as traffic_source_name,
            traffic_source.medium as traffic_source_medium,
            traffic_source.source as traffic_source_source,
            stream_id,
            platform,
            ecommerce,
            items,
            (
    select value.int_value
    from unnest(event_params)
    where key = 'ga_session_id'
) as ga_session_id,
            (
    select value.string_value
    from unnest(event_params)
    where key = 'page_location'
) as page_location,
            (
    select value.int_value
    from unnest(event_params)
    where key = 'ga_session_number'
) as session_number,
            coalesce(
                (
                    select value.int_value
                    from unnest(event_params)
                    where key = "session_engaged"
                ),
                (
                    case
                        when
                            (
                                select value.string_value
                                from unnest(event_params)
                                where key = "session_engaged"
                            )
                            = "1"
                        then 1
                    end
                )
            ) as session_engaged,
            (
    select value.int_value
    from unnest(event_params)
    where key = 'engagement_time_msec'
) as engagement_time_msec,
            (
    select value.string_value
    from unnest(event_params)
    where key = 'page_title'
) as page_title,
            (
    select value.string_value
    from unnest(event_params)
    where key = 'page_referrer'
) as page_referrer,
            (
    select value.string_value
    from unnest(event_params)
    where key = 'source'
) as source,
            (
    select value.string_value
    from unnest(event_params)
    where key = 'medium'
) as medium,
            (
    select value.string_value
    from unnest(event_params)
    where key = 'campaign'
) as campaign,
            (
    select value.string_value
    from unnest(event_params)
    where key = 'content'
) as content,
            (
    select value.string_value
    from unnest(event_params)
    where key = 'term'
) as term,
            case when event_name = 'page_view' then 1 else 0 end as is_page_view,
            case when event_name = 'purchase' then 1 else 0 end as is_purchase
        from source
    )

select *
from renamed
qualify
    row_number() over (
        partition by
            event_date_dt,
            stream_id,
            user_pseudo_id,
            ga_session_id,
            event_name,
            event_timestamp,
            to_json_string(event_params)
    )
    = 1
    );
  
      

      -- 2. define partitions to update
      set (dbt_partitions_for_replacement) = (
          select as struct
              -- IGNORE NULLS: this needs to be aligned to _dbt_max_partition, which ignores null
              array_agg(distinct date(event_date_dt) IGNORE NULLS)
          from `merkle-id-cdp`.`dbt_test_staging`.`base_ga4__events__dbt_tmp`
      );

      -- 3. run the merge statement
      

    merge into `merkle-id-cdp`.`dbt_test_staging`.`base_ga4__events` as DBT_INTERNAL_DEST
        using (
        select
        * from `merkle-id-cdp`.`dbt_test_staging`.`base_ga4__events__dbt_tmp`
      ) as DBT_INTERNAL_SOURCE
        on FALSE

    when not matched by source
         and date(DBT_INTERNAL_DEST.event_date_dt) in unnest(dbt_partitions_for_replacement) 
        then delete

    when not matched then insert
        (`event_date_dt`, `event_timestamp`, `event_name`, `event_params`, `event_previous_timestamp`, `event_value_in_usd`, `event_bundle_sequence_id`, `event_server_timestamp_offset`, `user_id`, `user_pseudo_id`, `privacy_info_analytics_storage`, `privacy_info_ads_storage`, `privacy_info_uses_transient_token`, `user_properties`, `user_first_touch_timestamp`, `user_ltv_revenue`, `user_ltv_currency`, `device_category`, `device_mobile_brand_name`, `device_mobile_model_name`, `device_mobile_marketing_name`, `device_mobile_os_hardware_model`, `device_operating_system`, `device_operating_system_version`, `device_vendor_id`, `device_advertising_id`, `device_language`, `device_is_limited_ad_tracking`, `device_time_zone_offset_seconds`, `device_web_info_browser`, `device_web_info_browser_version`, `geo_continent`, `geo_country`, `geo_region`, `geo_city`, `geo_sub_continent`, `geo_metro`, `app_info_id`, `app_info_version`, `app_info_install_store`, `app_info_firebase_app_id`, `app_info_install_source`, `traffic_source_name`, `traffic_source_medium`, `traffic_source_source`, `stream_id`, `platform`, `ecommerce`, `items`, `ga_session_id`, `page_location`, `session_number`, `session_engaged`, `engagement_time_msec`, `page_title`, `page_referrer`, `source`, `medium`, `campaign`, `content`, `term`, `is_page_view`, `is_purchase`)
    values
        (`event_date_dt`, `event_timestamp`, `event_name`, `event_params`, `event_previous_timestamp`, `event_value_in_usd`, `event_bundle_sequence_id`, `event_server_timestamp_offset`, `user_id`, `user_pseudo_id`, `privacy_info_analytics_storage`, `privacy_info_ads_storage`, `privacy_info_uses_transient_token`, `user_properties`, `user_first_touch_timestamp`, `user_ltv_revenue`, `user_ltv_currency`, `device_category`, `device_mobile_brand_name`, `device_mobile_model_name`, `device_mobile_marketing_name`, `device_mobile_os_hardware_model`, `device_operating_system`, `device_operating_system_version`, `device_vendor_id`, `device_advertising_id`, `device_language`, `device_is_limited_ad_tracking`, `device_time_zone_offset_seconds`, `device_web_info_browser`, `device_web_info_browser_version`, `geo_continent`, `geo_country`, `geo_region`, `geo_city`, `geo_sub_continent`, `geo_metro`, `app_info_id`, `app_info_version`, `app_info_install_store`, `app_info_firebase_app_id`, `app_info_install_source`, `traffic_source_name`, `traffic_source_medium`, `traffic_source_source`, `stream_id`, `platform`, `ecommerce`, `items`, `ga_session_id`, `page_location`, `session_number`, `session_engaged`, `engagement_time_msec`, `page_title`, `page_referrer`, `source`, `medium`, `campaign`, `content`, `term`, `is_page_view`, `is_purchase`)

;

      -- 4. clean up the temp table
      drop table if exists `merkle-id-cdp`.`dbt_test_staging`.`base_ga4__events__dbt_tmp`

  


  

    