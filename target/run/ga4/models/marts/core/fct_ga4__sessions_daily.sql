
   
      -- generated script to merge partitions into `merkle-id-cdp`.`dbt_test_marts`.`fct_ga4__sessions_daily`
      declare dbt_partitions_for_replacement array<date>;

      
      
        declare _dbt_max_partition date default (
      select max(session_partition_date) from `merkle-id-cdp`.`dbt_test_marts`.`fct_ga4__sessions_daily`
      where session_partition_date is not null
    );

       -- 1. create a temp table with model data
        
  
    

    create or replace table `merkle-id-cdp`.`dbt_test_marts`.`fct_ga4__sessions_daily__dbt_tmp`
    partition by session_partition_date
    

    OPTIONS(
      expiration_timestamp=TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 12 hour)
    )
    as (
      

with
    session_metrics as (
        select
            session_key,
            session_partition_key,
            user_pseudo_id,
            min(event_date_dt) as session_partition_date,  -- Used only as a method of partitioning sessions within this incremental table. Does not represent the true session start date
            min(event_timestamp) as session_partition_min_timestamp,
            countif(event_name = 'page_view') as session_partition_count_page_views,
            countif(event_name = 'purchase') as session_partition_count_purchases,
            sum(event_value_in_usd) as session_partition_sum_event_value_in_usd,
            ifnull(max(session_engaged), 0) as session_partition_max_session_engaged,
            sum(engagement_time_msec) as session_partition_sum_engagement_time_msec
        from `merkle-id-cdp`.`dbt_test_staging`.`stg_ga4__events`
        -- Give 1 extra day to ensure we beging aggregation at the start of a session
        where
            session_key is not null
            
            and event_date_dt >= date_sub(_dbt_max_partition, interval 1 day)
            
        group by 1, 2, 3
    )
 select * from session_metrics

    );
  
      

      -- 2. define partitions to update
      set (dbt_partitions_for_replacement) = (
          select as struct
              -- IGNORE NULLS: this needs to be aligned to _dbt_max_partition, which ignores null
              array_agg(distinct date(session_partition_date) IGNORE NULLS)
          from `merkle-id-cdp`.`dbt_test_marts`.`fct_ga4__sessions_daily__dbt_tmp`
      );

      -- 3. run the merge statement
      

    merge into `merkle-id-cdp`.`dbt_test_marts`.`fct_ga4__sessions_daily` as DBT_INTERNAL_DEST
        using (
        select
        * from `merkle-id-cdp`.`dbt_test_marts`.`fct_ga4__sessions_daily__dbt_tmp`
      ) as DBT_INTERNAL_SOURCE
        on FALSE

    when not matched by source
         and date(DBT_INTERNAL_DEST.session_partition_date) in unnest(dbt_partitions_for_replacement) 
        then delete

    when not matched then insert
        (`session_key`, `session_partition_key`, `user_pseudo_id`, `session_partition_date`, `session_partition_min_timestamp`, `session_partition_count_page_views`, `session_partition_count_purchases`, `session_partition_sum_event_value_in_usd`, `session_partition_max_session_engaged`, `session_partition_sum_engagement_time_msec`)
    values
        (`session_key`, `session_partition_key`, `user_pseudo_id`, `session_partition_date`, `session_partition_min_timestamp`, `session_partition_count_page_views`, `session_partition_count_purchases`, `session_partition_sum_event_value_in_usd`, `session_partition_max_session_engaged`, `session_partition_sum_engagement_time_msec`)

;

      -- 4. clean up the temp table
      drop table if exists `merkle-id-cdp`.`dbt_test_marts`.`fct_ga4__sessions_daily__dbt_tmp`

  


  

    