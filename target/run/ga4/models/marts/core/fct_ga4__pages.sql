
   
      -- generated script to merge partitions into `merkle-id-cdp`.`dbt_test_marts`.`fct_ga4__pages`
      declare dbt_partitions_for_replacement array<date>;

      
      
        

       -- 1. create a temp table with model data
        
  
    

    create or replace table `merkle-id-cdp`.`dbt_test_marts`.`fct_ga4__pages__dbt_tmp`
    partition by event_date_dt
    

    OPTIONS(
      expiration_timestamp=TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 12 hour)
    )
    as (
      

with
    page_view as (
        select
            event_date_dt,
            extract(hour from (select timestamp_micros(event_timestamp))) as hour,
            page_location,  -- includes query string parameters not listed in query_parameter_exclusions variable
            page_key,
            page_path,
            page_title,  -- would like to move this to dim_ga4__pages but need to think how to handle page_title changing over time
            count(event_name) as page_views,
            count(distinct user_pseudo_id) as distinct_user_pseudo_ids,
            sum(if(session_number = 1, 1, 0)) as new_user_pseudo_ids,
            sum(entrances) as entrances,
            sum(engagement_time_msec) as total_time_on_page

        from `merkle-id-cdp`.`dbt_test_staging`.`stg_ga4__event_page_view`
        group by 1, 2, 3, 4, 5, 6
    ),
    scroll as (
        select
            event_date_dt,
            extract(hour from (select timestamp_micros(event_timestamp))) as hour,
            page_location,
            page_title,
            count(event_name) as scroll_events
        from `merkle-id-cdp`.`dbt_test_staging`.`stg_ga4__event_scroll`
        group by 1, 2, 3, 4
    )

select page_view.* except (page_key), ifnull(scroll.scroll_events, 0) as scroll_events
from page_view
left join scroll using (event_date_dt, hour, page_location, page_title)

    );
  
      

      -- 2. define partitions to update
      set (dbt_partitions_for_replacement) = (
          select as struct
              -- IGNORE NULLS: this needs to be aligned to _dbt_max_partition, which ignores null
              array_agg(distinct date(event_date_dt) IGNORE NULLS)
          from `merkle-id-cdp`.`dbt_test_marts`.`fct_ga4__pages__dbt_tmp`
      );

      -- 3. run the merge statement
      

    merge into `merkle-id-cdp`.`dbt_test_marts`.`fct_ga4__pages` as DBT_INTERNAL_DEST
        using (
        select
        * from `merkle-id-cdp`.`dbt_test_marts`.`fct_ga4__pages__dbt_tmp`
      ) as DBT_INTERNAL_SOURCE
        on FALSE

    when not matched by source
         and date(DBT_INTERNAL_DEST.event_date_dt) in unnest(dbt_partitions_for_replacement) 
        then delete

    when not matched then insert
        (`event_date_dt`, `hour`, `page_location`, `page_path`, `page_title`, `page_views`, `distinct_user_pseudo_ids`, `new_user_pseudo_ids`, `entrances`, `total_time_on_page`, `scroll_events`)
    values
        (`event_date_dt`, `hour`, `page_location`, `page_path`, `page_title`, `page_views`, `distinct_user_pseudo_ids`, `new_user_pseudo_ids`, `entrances`, `total_time_on_page`, `scroll_events`)

;

      -- 4. clean up the temp table
      drop table if exists `merkle-id-cdp`.`dbt_test_marts`.`fct_ga4__pages__dbt_tmp`

  


  

    