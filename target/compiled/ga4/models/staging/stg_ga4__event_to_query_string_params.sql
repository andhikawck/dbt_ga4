with
    event_and_query_string as (
        select event_key, split(page_query_string, '&') as qs_split
        from `merkle-id-cdp`.`dbt_test_staging`.`stg_ga4__events`
    ),
    flattened_qs as (
        select event_key, params from event_and_query_string, unnest(qs_split) as params
    ),
    split_param_value as (
        select
            event_key,
            split(params, '=')[safe_offset(0)] as param,
            nullif(split(params, '=')[safe_offset(1)], '') as value
        from flattened_qs
    )

select *
from split_param_value