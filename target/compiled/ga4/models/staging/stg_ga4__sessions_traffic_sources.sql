with
    session_events as (
        select
            session_key,
            event_timestamp,
            lower(source) as source,
            medium,
            campaign,
            content,
            term,
            source_category
        from `merkle-id-cdp`.`dbt_test_staging`.`stg_ga4__events`
        left join `merkle-id-cdp`.`dbt_test_seeds`.`ga4_source_categories` using (source)
        where
            session_key is not null
            and event_name != 'session_start'
            and event_name != 'first_visit'
    ),
    set_default_channel_grouping as (
        select
            *,
            
case
    when source is null and medium is null
    then 'Direct'
    when
        source = '(direct)'
        and (medium = '(none)' or medium = '(not set)')
    then 'Direct'

    when
        regexp_contains(
            source, r"^(facebook|instagram|pinterest|reddit|twitter|linkedin)"
        )
        = true
        and regexp_contains(medium, r"^(cpc|ppc|paid)") = true
    then 'Paid Social'
    when
        regexp_contains(
            source, r"^(facebook|instagram|pinterest|reddit|twitter|linkedin)"
        )
        = true
        or regexp_contains(
            medium,
            r"^(social|social-network|social-media|sm|social network|social media)"
        )
        = true
        or source_category = 'SOURCE_CATEGORY_SOCIAL'
    then 'Organic Social'
    when
        regexp_contains(medium, r"email|e-mail|e_mail|e mail") = true
        or regexp_contains(source, r"email|e-mail|e_mail|e mail") = true
    then 'Email'
    when regexp_contains(medium, r"affiliate|affiliates") = true
    then 'Affiliates'
    when
        source_category = 'SOURCE_CATEGORY_SHOPPING'
        and regexp_contains(medium, r"^(.*cp.*|ppc|paid.*)$")
    then 'Paid Shopping'
    when
        (
            source_category = 'SOURCE_CATEGORY_VIDEO'
            and regexp_contains(medium, r"^(.*cp.*|ppc|paid.*)$")
        )
        or source = 'dv360_video'
    then 'Paid Video'
    when
        regexp_contains(medium, r"^(display|cpm|banner)$")
        or source = 'dv360_display'
    then 'Display'
    when regexp_contains(medium, r"^(cpc|ppc|paidsearch)$")
    then 'Paid Search'
    when regexp_contains(medium, r"^(cpv|cpa|cpp|content-text)$")
    then 'Other Advertising'
    when medium = 'organic' or source_category = 'SOURCE_CATEGORY_SEARCH'
    then 'Organic Search'
    when
        regexp_contains(medium, r"^(.*video.*)$")
        or source_category = 'SOURCE_CATEGORY_VIDEO'
    then 'Organic Video'
    when source_category = 'SOURCE_CATEGORY_SHOPPING'
    then 'Organic Shopping'
    when medium = 'referral'
    then 'Referral'
    when medium = 'audio'
    then 'Audio'
    when medium = 'sms'
    then 'SMS'
    else '(Other)'
end


            as default_channel_grouping
        from session_events
    ),
    session_source as (
        select
            session_key,
            coalesce(
                first_value(
                    (case when source <> '(direct)' then source end) ignore nulls
                ) over (session_window),
                '(direct)'
            ) as source,
            coalesce(
                first_value(
                    (
                        case
                            when source <> '(direct)' then coalesce(medium, '(none)')
                        end
                    ) ignore nulls
                ) over (session_window),
                '(none)'
            ) as medium,
            coalesce(
                first_value(
                    (
                        case
                            when source <> '(direct)'
                            then coalesce(source_category, '(none)')
                        end
                    ) ignore nulls
                ) over (session_window),
                '(none)'
            ) as source_category,
            coalesce(
                first_value(
                    (
                        case
                            when source <> '(direct)' then coalesce(campaign, '(none)')
                        end
                    ) ignore nulls
                ) over (session_window),
                '(none)'
            ) as campaign,
            coalesce(
                first_value(
                    (
                        case
                            when source <> '(direct)' then coalesce(content, '(none)')
                        end
                    ) ignore nulls
                ) over (session_window),
                '(none)'
            ) as content,
            coalesce(
                first_value(
                    (
                        case when source <> '(direct)' then coalesce(term, '(none)') end
                    ) ignore nulls
                ) over (session_window),
                '(none)'
            ) as term,
            coalesce(
                first_value(
                    (
                        case
                            when source <> '(direct)'
                            then coalesce(default_channel_grouping, '(none)')
                        end
                    ) ignore nulls
                ) over (session_window),
                '(none)'
            ) as default_channel_grouping
        from set_default_channel_grouping
        window
            session_window as (
                partition by session_key
                order by event_timestamp asc
                rows between unbounded preceding and unbounded following
            )
    )

select distinct *
from session_source