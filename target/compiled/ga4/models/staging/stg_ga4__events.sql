-- This staging model contains key creation and window functions. Keeping window
-- functions outside of the base incremental model ensures that the incremental
-- updates don't artificially limit the window partition sizes (ex: if a session spans
-- 2 days, but only 1 day is in the incremental update)
with
    base_events as (
        select *
        from `merkle-id-cdp`.`dbt_test_staging`.`base_ga4__events`
        
    ),
    -- Add unique key for sessions. session_key will be null if user_pseudo_id is null
    -- due to consent being denied. ga_session_id may be null during audience trigger
    -- events.
    include_session_key as (
        select
            *,
            to_base64(
                md5(concat(stream_id, user_pseudo_id, cast(ga_session_id as string)))
            ) as session_key  -- Surrogate key to determine unique session across streams and users. Sessions do NOT reset after midnight in GA4
        from base_events
    ),
    -- Add a key that combines session key and date. Useful when working with session
    -- table within date-partitioned tables
    include_session_partition_key as (
        select
            *,
            concat(session_key, cast(event_date_dt as string)) as session_partition_key
        from include_session_key
    ),
    -- Add unique key for events
    include_event_key as (
        select
            *,
            to_base64(
                md5(
                    concat(
                        session_key,
                        event_name,
                        cast(event_timestamp as string),
                        to_json_string(event_params)
                    )
                )
            ) as event_key  -- Surrogate key for unique events.  
        from include_session_partition_key
    ),
    detect_gclid as (
        select
            * except (source, medium, campaign),
            case
                when (page_location like '%gclid%' and source is null)
                then "google"
                else source
            end as source,
            case
                when (page_location like '%gclid%' and medium is null)
                then "cpc"
                else medium
            end as medium,
            case
                when (page_location like '%gclid%' and campaign is null)
                then "(cpc)"
                else campaign
            end as campaign
        from include_event_key
    ),
    -- Remove specific query strings from page_location field
    remove_query_params as (

        select
            * except (page_location, page_referrer),
            page_location as original_page_location,
            page_referrer as original_page_referrer,
            
regexp_extract(page_location, '(?:\\w+:)?\\/\\/[^\\/]+([^?#]+)')
 as page_path,
            -- If there are query parameters to exclude, exclude them using regex
             page_location, page_referrer
            
        from detect_gclid
    ),
    enrich_params as (
        select
            *,
            
regexp_extract(page_location, '(?:http[s]?://)?(?:www\\.)?(.*?)(?:(?:/|:)(?:.)*|$)')
 as page_hostname,
            
regexp_extract(page_location, '\\?(.+)')
 as page_query_string,
        from remove_query_params
    ),
    page_key as (
        select
            *,
            (
                concat(
                    cast(event_date_dt as string),
                    cast(
                        extract(hour from timestamp_micros(event_timestamp)) as string
                    ),
                    page_location
                )
            ) as page_key
        from enrich_params
    )
select *
from page_key