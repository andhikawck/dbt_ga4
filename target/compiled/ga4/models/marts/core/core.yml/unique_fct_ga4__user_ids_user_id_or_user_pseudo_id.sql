
    
    

with dbt_test__target as (

  select user_id_or_user_pseudo_id as unique_field
  from `merkle-id-cdp`.`dbt_test_marts`.`fct_ga4__user_ids`
  where user_id_or_user_pseudo_id is not null

)

select
    unique_field,
    count(*) as n_records

from dbt_test__target
group by unique_field
having count(*) > 1


