

with
    session_metrics as (
        select
            session_key,
            session_partition_key,
            user_pseudo_id,
            min(event_date_dt) as session_partition_date,  -- Used only as a method of partitioning sessions within this incremental table. Does not represent the true session start date
            min(event_timestamp) as session_partition_min_timestamp,
            countif(event_name = 'page_view') as session_partition_count_page_views,
            countif(event_name = 'purchase') as session_partition_count_purchases,
            sum(event_value_in_usd) as session_partition_sum_event_value_in_usd,
            ifnull(max(session_engaged), 0) as session_partition_max_session_engaged,
            sum(engagement_time_msec) as session_partition_sum_engagement_time_msec
        from `merkle-id-cdp`.`dbt_test_staging`.`stg_ga4__events`
        -- Give 1 extra day to ensure we beging aggregation at the start of a session
        where
            session_key is not null
            
            and event_date_dt >= date_sub(_dbt_max_partition, interval 1 day)
            
        group by 1, 2, 3
    )
 select * from session_metrics
