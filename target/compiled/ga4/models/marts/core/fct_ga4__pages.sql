

with
    page_view as (
        select
            event_date_dt,
            extract(hour from (select timestamp_micros(event_timestamp))) as hour,
            page_location,  -- includes query string parameters not listed in query_parameter_exclusions variable
            page_key,
            page_path,
            page_title,  -- would like to move this to dim_ga4__pages but need to think how to handle page_title changing over time
            count(event_name) as page_views,
            count(distinct user_pseudo_id) as distinct_user_pseudo_ids,
            sum(if(session_number = 1, 1, 0)) as new_user_pseudo_ids,
            sum(entrances) as entrances,
            sum(engagement_time_msec) as total_time_on_page

        from `merkle-id-cdp`.`dbt_test_staging`.`stg_ga4__event_page_view`
        group by 1, 2, 3, 4, 5, 6
    ),
    scroll as (
        select
            event_date_dt,
            extract(hour from (select timestamp_micros(event_timestamp))) as hour,
            page_location,
            page_title,
            count(event_name) as scroll_events
        from `merkle-id-cdp`.`dbt_test_staging`.`stg_ga4__event_scroll`
        group by 1, 2, 3, 4
    )

select page_view.* except (page_key), ifnull(scroll.scroll_events, 0) as scroll_events
from page_view
left join scroll using (event_date_dt, hour, page_location, page_title)
