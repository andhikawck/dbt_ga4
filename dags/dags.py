import datetime
import json

from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.utils.dates import datetime
from airflow.utils.dates import timedelta

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2023, 4, 12),
    'email': ['maria.benedicta@dentsu.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

DBT_DIR = "dags/dbt-ga4"

def load_manifest():
    local_filepath = "dags/dbt-ga4/target/manifest.json"
    with open(local_filepath) as f:
        data = json.load(f)

    return data

data = load_manifest()
                
with DAG(
    "dbt_advanced_dag",
    start_date=datetime(2020, 12, 23),
    description="A dbt wrapper for Airflow.",
    schedule_interval=None,
    catchup=False,
    doc_md=__doc__
) as dag:
    
    def load_manifest():
        local_filepath = f"{DBT_DIR}/target/manifest.json"
        with open(local_filepath) as f:
            data = json.load(f)
        return data

    def make_dbt_task(node, dbt_verb):
        DBT_DIR = "dbt-ga4"
        GLOBAL_CLI_FLAGS = "--no-write-json"
        model = node.split(".")[-1]

        if dbt_verb == "run":
            dbt_task = BashOperator(
                task_id=node,
                bash_command=f"""
                cd {DBT_DIR} &&
                dbt {GLOBAL_CLI_FLAGS} {dbt_verb} --target prod --models {model}
                """,
                dag=dag,
            )

        elif dbt_verb == "test":
            node_test = node.replace("model", "test")
            dbt_task = BashOperator(
                task_id=node_test,
                bash_command=f"""
                cd {DBT_DIR} &&
                dbt {GLOBAL_CLI_FLAGS} {dbt_verb} --target prod --models {model}
                """,
                dag=dag,
            )

        return dbt_task

    data = load_manifest()
    dbt_tasks = {}

    for node in data["nodes"].keys():
        if node.split(".")[0] == "model":
            node_test = node.replace("model", "test")
            dbt_tasks[node] = make_dbt_task(node, "run")
            dbt_tasks[node_test] = make_dbt_task(node, "test")

    for node in data["nodes"].keys():
        if node.split(".")[0] == "model":
            # Set dependency to run tests on a model after model runs finishes
            node_test = node.replace("model", "test")
            dbt_tasks[node] >> dbt_tasks[node_test]
            # Set all model -> model dependencies
            for upstream_node in data["nodes"][node]["depends_on"]["nodes"]:
                upstream_node_type = upstream_node.split(".")[0]
                if upstream_node_type == "model":
                    dbt_tasks[upstream_node] >> dbt_tasks[node]