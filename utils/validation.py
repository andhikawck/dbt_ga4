from flask import abort, make_response, jsonify, request
from marshmallow import ValidationError
from utils.config import AUTH_WRITE_KEY
import functools


def validate_schema(schema, data):
    try:
        return schema.load(data)
    except ValidationError as e:
        abort(make_response(jsonify(Status="Failed", Message=str(e.messages)), 400))


def write_key_required(func):
    @functools.wraps(func)
    def decorator(*args, **kwargs):
        if request.headers.get("hashkey"):
            write_key = request.headers.get("hashkey")
        else:
            return {"Status": "Unauthorized", "Message": "Key needed on headers"}, 401

        # Check if API key is correct and valid
        if write_key == AUTH_WRITE_KEY:
            return func(*args, **kwargs)
        else:
            return {
                "Status": "Unauthorized",
                "Message": "The provided API key is not valid",
            }, 403

    return decorator
